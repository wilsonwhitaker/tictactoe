<?php

// A simple game of tic tac toe

class WW_TicTacToe
{

	const SESSION_NAME = 'tictactoe';
	protected $storage;
	protected $active_player;
	protected $size = 3;
	protected $winner = false;
	protected $winning_number,$winning_type;

	public function __construct()
	{
		$this->_init_storage();
	}

	public function __destruct()
	{
		$this->_save_storage();
	}

	/*
	 * Init simple session storage
	 */
	protected function _init_storage()
	{
		session_start();

		if (!isset($_SESSION[self::SESSION_NAME])){
			$this->storage = new stdClass();
			$this->storage->spots = array();
	
		} else {
			$this->storage = $_SESSION[self::SESSION_NAME];
		}
	}

	protected function _save_storage()
	{
		$_SESSION[self::SESSION_NAME] = $this->storage;
	}

	protected function _destroy_storage()
	{
		if ( isset($_SESSION[self::SESSION_NAME]) ){
			$this->storage = NULL;
			unset($_SESSION[self::SESSION_NAME]);
		}
	}

	/*
	 * Allow the game to start over
	 */
	public function reset()
	{
		$this->_destroy_storage();
		$this->go_home();
	}

	/*
	 * Refresh page to remove query string
	 */
	protected function go_home()
	{
		$uri = explode('?', $_SERVER['REQUEST_URI']);
		header('location:'.$uri[0]);
	}

	/*
	 * Get board dimensions
	 */
	public function get_size()
	{
		return $this->size;
	}

	/*
	 * Get the player making this move
	 */
	public function get_active_player()
	{
		if (!$this->storage->active_player) $this->set_active_player('x');

		return $this->storage->active_player;
	}

	public function set_active_player($player)
	{
		if( in_array($player, array('x','o')) )
			$this->storage->active_player = $player;
	}

	protected function switch_player()
	{
		if ($this->get_active_player() == 'x')
			$this->set_active_player('o');
		else $this->set_active_player('x');

	}

	/*
	 * Check to see if this square is already occupied
	 */
	public function get_spot_owner($row,$column)
	{
		if (isset($this->storage->spots[$row][$column])){
			return $this->storage->spots[$row][$column];
		}
	}

	public function set_spot_owner($row,$column,$player)
	{
		$this->storage->spots[$row][$column] = $player;
	}

	public function get_turn_count()
	{
		return isset($this->storage->turns) ? $this->storage->turns : 0;
	}
	public function set_turn_count($value)
	{
		$this->storage->turns = $value;
	}

	/*
	 * Place the mark and check if there is a winner yet.
	 */
	public function play_turn($options=array())
	{

		if($this->get_winner())
			throw new Exception("Game is won!", 1);

		$row = isset($options['r']) ? $options['r'] : false;
		$column = isset($options['c']) ? $options['c'] : false;
		$player = isset($options['p']) ? $options['p'] : false;

		if($row === false or $player === false or $column === false)
			throw new Exception("missing inputs", 1);

		if (isset($this->storage->spots[$row][$column])){
			throw new Exception("Spot already taken", 1);
		}

		// TODO: handle verification of input

		$this->set_spot_owner($row,$column,$player);
		$this->calculate_score($row,$column,$player);
		$this->set_turn_count( $this->get_turn_count() + 1);
		
		if(!$this->get_winner()){
			$this->draw();
			$this->switch_player();
		}

		$this->go_home();
			
	}

	public function get_winner()
	{
		return isset($this->storage->winner) ? $this->storage->winner : false;
	}

	protected function set_winner($winner)
	{
		$this->storage->winner = $winner;
		$this->winner = $winner;
	}

	public function get_winning_number()
	{
		if (!$this->winning_number){
			$this->get_winning_type();
		}

		return $this->winning_number;
	}

	public function get_winning_type()
	{
		if (!$this->winning_type){
			$streak = explode('-',$this->get_streak());
			$this->winning_type = $streak[0];
			$this->winning_number = $streak[1];
		}

		return $this->winning_type;
	}

	/*
	 * Get winning row, column or diag
	 */
	public function get_streak()
	{
		return isset($this->storage->streak) ? $this->storage->streak : false;
	}

	protected function set_streak($streak)
	{
		$this->storage->streak = $streak;
	}

	/*
	 * Calculate winning row, etc
	 */
	public function calculate_score($row,$column,$player)
	{

		if($this->winner)
			return;

		$this->increment_score($row,$column,$player);

		$winner = false;

		if ($this->storage->score[$player]['row'][$row] == $this->size)
			$winner = "row-$row";

		if ($this->storage->score[$player]['column'][$column] == $this->size){
			$winner = "column-$column";
		}

		if (
			$row == $column and
			$this->storage->score[$player]['diag'][0] == $this->size
		)
			$winner = "diag-0";

		if (
			$this->is_reverse_diag($row,$column) and 
			$this->storage->score[$player]['diag'][1] == $this->size
		)
			$winner = "diag-1";
		
		if($winner){
			$this->set_winner($this->get_active_player());
			$this->set_streak($winner);
		}

		return $winner;
	}

	/*
	 * Check to see if both players have used all their moves
	 */
	protected function draw()
	{

		if ( $this->get_draw() ) return true;

		if ( $this->get_turn_count() >= pow($this->size,2)){
			
			$this->set_draw(true);
			return true;
		}
		return false;

	}

	public function set_draw($draw)
	{
		$this->storage->draw = $draw;
	}

	public function get_draw()
	{
		return isset($this->storage->draw) ? $this->storage->draw : false;
	}

	protected function increment_score($row,$column,$player)
	{
		if($this->winner)
			return;

		if ( !isset($this->storage->score[$player]['row'][$row]) ) $this->storage->score[$player]['row'][$row] = 0;
		$this->storage->score[$player]['row'][$row]++;

		if ( !isset($this->storage->score[$player]['column'][$column]) ) $this->storage->score[$player]['column'][$column] = 0;
		$this->storage->score[$player]['column'][$column]++;

		if ( !isset($this->storage->score[$player]['diag'][0]) ){
			$this->storage->score[$player]['diag'][0] = 0;
			$this->storage->score[$player]['diag'][1] = 0;
		}

		if($row == $column)
			$this->storage->score[$player]['diag'][0]++;

		if($this->is_reverse_diag($row,$column))
			$this->storage->score[$player]['diag'][1]++;
	}

	/*
	 * Check to see if this is part of a reverse diag
	 */
	public function is_reverse_diag($row,$column)
	{
		for ($i=0; $i < $this->size; $i++) { 
			if (
				$row == $i and
				$column == ($this->size -1 -$i)
			)
				return true;
		}
		return false;
	}

}

function disp_r($value)
{
	echo '<pre>';
	print_r($value);
	echo '</pre>';
}