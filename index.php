<?

include_once('lib/WW_TicTacToe.php');
$ttt = new WW_TicTacToe;

$uri = explode('?', $_SERVER['REQUEST_URI']);

if ( isset($_REQUEST['play']) && $_REQUEST['play'] == 'go' ){
	
	try{
		$ttt->play_turn($_REQUEST);
	}catch(Exception $e){
		// TODO: add error messages
	}
}

if(isset($_REQUEST['reset']))
	$ttt->reset();

?><!DOCTYPE html>
<head>
	<title>WW Tic-Tac-Toe</title>

	<link rel="stylesheet" type="text/css" href="css/style.css">
	
	<style>
	td{
		width: <?=100/$ttt->get_size()?>%;
	}
	.winner.column td:nth-child(<?=$ttt->get_winning_number()+1?>) a,
	.winner.row tr:nth-child(<?=$ttt->get_winning_number()+1?>) a,
	.winner.diag-0 .diag-0 a,
	.winner.diag-1 .diag-1 a
	{
		color: white;
		background: #5cb85c;
	}
	</style>

</head>
<body>

	<div class="container">
		<div class="players">
		<?if( $ttt->get_winner() ){?>
			Congrats, <?=ucfirst($ttt->get_active_player())?>!
		<?}elseif($ttt->get_draw()){?>
			Shucks! It's a draw.
		<?}else{?>
			Your turn, <span class="player active"><?=ucfirst($ttt->get_active_player())?>!</span>
		<?}?>
		</div>
		<table class="table <?=$ttt->get_winner() ? 'winner '.$ttt->get_winning_type().' '.$ttt->get_streak() : ''?>">
			<?php for ($row=0; $row < $ttt->get_size(); $row++) { ?>
			<tr>
				<?php for ($col=0; $col < $ttt->get_size(); $col++) { 
					
					$owner = $ttt->get_spot_owner($row,$col);

					$classes = array();
					if($row==$col)
						$classes[] = 'diag-0';
					if($ttt->is_reverse_diag($row,$col))
						$classes[] = 'diag-1';
					?>
					<td class="<?=implode(' ', $classes)?>">
						<a class="ttt" href="<?if(!$ttt->get_winner() and !$ttt->get_draw()){
							?>?play=go&amp;r=<?=$row?>&amp;c=<?=$col?>&amp;p=<?=$ttt->get_active_player()?><?
							}else echo '#'?>">
							<span class="spot spot-<?=$owner?>"><?=$owner ? $owner : $ttt->get_active_player()?></span>
						</a>
					</td>	
				<?}?>
			</tr>
			<?}?>
		</table>
		<a href="?reset=reset">Reset</a>
	</div>

</body>
</html>